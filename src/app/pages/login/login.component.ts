import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from './login.service';
import swal from 'sweetalert2';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public loginModel = {
    email: "",
    password: "",
  };
  constructor(private router: Router,private loginService: LoginService) {}

  ngOnInit() {}

  login() {
    this.loginService.loginUser(this.loginModel).subscribe(
      (data) => {
        if(data['statusCode'] === 200){
          console.log("=============roleId===========>",data['data'])
          sessionStorage.setItem("token", data['token']);
          localStorage.setItem("token", data['token']);
          localStorage.setItem("userId", data['data'].id);
          localStorage.setItem("userEmail", data['data'].email);
          localStorage.setItem("roleCode", data['data'].roleCode);
          this.userLoginsuccessAlertTimer();
          this.router.navigate(["/pages/adminDashboard/adminDashboard"]);
        }else if(data['statusCode'] === 201){
          swal("Error", data['message'], "error");
        }else if(data['statusCode'] === 202){
          swal("Error", data['message'], "error");
        }else if(data['statusCode'] === 203){
          swal("Error", data['message'], "error");
        }else if(data['statusCode'] === 404){
          swal("Error", data['message'], "error");
        }       
      },
      (error) => {
        return error;   
      });     
  }

  userLoginsuccessAlertTimer() {
      swal({
        type: 'success',
        title: 'Login successful',
        text: '',
        timer: 1000,
      });
  }

}
