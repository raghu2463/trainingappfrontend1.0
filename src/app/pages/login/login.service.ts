import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { User } from '../users/user.model';

@Injectable({
  providedIn: "root",
})
export class LoginService {
  constructor(private http: HttpClient) {}
  loginUser(req): Observable<User> {
    let body = req;
    return this.http.post<User>("api/login", body);
  }
}
