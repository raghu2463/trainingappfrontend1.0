import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-adminDashboard',
    template: `<router-outlet></router-outlet>`
})
export class AdminDashboardComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
