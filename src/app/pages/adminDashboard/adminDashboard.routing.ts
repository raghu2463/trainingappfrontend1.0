import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './adminDashboard.component';
import { AdminDashboardChaildComponent } from './components/echarts/adminDashboardChaild.component';

const childRoutes: Routes = [
    {
        path: '',
        component: AdminDashboardComponent,
        children: [
            { path: '', redirectTo: 'adminDashboardChaild', pathMatch: 'full' },
            { path: 'adminDashboardChaild', component: AdminDashboardChaildComponent },
        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);