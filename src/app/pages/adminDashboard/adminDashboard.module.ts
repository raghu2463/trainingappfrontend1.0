import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { routing } from './adminDashboard.routing';
import { NgxEchartsModule } from 'ngx-echarts';

/* components */
import { AdminDashboardComponent } from './adminDashboard.component';
import { AdminDashboardChaildComponent } from './components/echarts/adminDashboardChaild.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        routing
    ],
    declarations: [
        AdminDashboardComponent,
        AdminDashboardChaildComponent
    ]
})
export class AdminDashboardModule { }
