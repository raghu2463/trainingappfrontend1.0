import { Component } from '@angular/core';
import { AdminDashboardService } from './adminDashboard.service';

@Component({
  selector: 'app-adminDashboardChaild',
  templateUrl: './adminDashboardChaild.component.html',
  styleUrls: ['./adminDashboardChaild.component.scss'],
  providers: [AdminDashboardService]
})
export class AdminDashboardChaildComponent {
  showloading: boolean = false;
  BarOption;
  LineOption;
  PieOption;
  AnimationBarOption;

  constructor(private adminDashboardChaildService: AdminDashboardService) {
    this.BarOption = this.adminDashboardChaildService.getBarOption();
    // this.LineOption = this.adminDashboardChaildService.getLineOption();
    this.PieOption = this.adminDashboardChaildService.getPieOption();
    this.AnimationBarOption = this.adminDashboardChaildService.getAnimationBarOption();
  }
}
