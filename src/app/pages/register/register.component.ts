import { Component, OnInit } from "@angular/core";
import { RegisterService } from './register.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  public registerModel = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    phoneNumber: "",
    headLine: "",
    userImage: "",
    linkedIn: "",
    github: "",
    youTube: "",
    roleId: "",
    status: "",
  };
  constructor(private registerService:RegisterService,private router:Router) {}

  ngOnInit() {}

  register() {
    this.registerService.registerUser(this.registerModel).subscribe(
      (data) => {
        if(data['statusCode'] === 304){
          swal("Error", data['message'], "error");
        }else if(data['statusCode'] === 200){
          swal("Success", data['message'], "success");
          this.router.navigate(["/"]);
        }
      },
      (error) => {
        return error;   
      });  
  }
}
