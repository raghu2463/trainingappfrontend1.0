import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { User } from '../users/user.model';

@Injectable({
  providedIn: "root",
})
export class RegisterService {
  constructor(private http: HttpClient) {}
  registerUser(req): Observable<User> {
    let body = req;
    return this.http.post<User>("api/register", body);
  }
}
