import { Routes, RouterModule } from '@angular/router';
import { CertificateComponent } from './certificate.component';

const childRoutes: Routes = [
    {
        path: '',
        component: CertificateComponent,
        children: [
            { path: '', redirectTo: '', pathMatch: 'full' },
            { path: 'certificate', component: CertificateComponent },
        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
