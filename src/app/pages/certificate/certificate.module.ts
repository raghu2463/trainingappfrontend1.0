import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './certificate.routing';
import { SharedModule } from '../../shared/shared.module';
import { CertificateComponent } from './certificate.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        routing
    ],
    declarations: [
        CertificateComponent
    ]
})
export class CertificateModule { }
