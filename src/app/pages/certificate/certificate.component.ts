
import { Component, OnInit } from "@angular/core";
import { UsersService } from "../users/users.service";

@Component({
    selector: "app-certificate",
    templateUrl: "./certificate.component.html",
    styleUrls: ["./certificate.component.scss"],
  })

  export class CertificateComponent implements OnInit {
      users: {};
    constructor(
        private userService: UsersService
      ) {
          this.users ={};
      }
    ngOnInit() {
        var data = sessionStorage.getItem('id');
        console.log('session',data) ;
        this.userService.getViewUser(data).subscribe(
            (data) => {
console.log('data',data);
this.users = data["data"];
console.log('users',this.users);
    });
    }
  }