export interface Courses {
    id?: string;

    courseNmae?: string;

    courseCode?: string;

    category?: string;

    courseDescription?: string;

    language?: string; 

    courseDuration?: string;

    byUserId?: string;

    status?: string;

    createdAt?: string;   

    updatedAt?: Date;

    fromDate?: Date;

    toDate?: Date;
}