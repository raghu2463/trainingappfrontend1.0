import { Routes, RouterModule } from '@angular/router';
import { CoursesComponent } from './courses.component';

const childRoutes: Routes = [
    {
        path: '',
        component: CoursesComponent
    }
];

export const UsersRouting = RouterModule.forChild(childRoutes);
