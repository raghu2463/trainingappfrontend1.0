import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Courses } from './courses.model';
import {SERVER_API_URL} from '../../constant'

@Injectable({
    providedIn: 'root'
  })
  
  export class CoursesService {
    constructor(private http: HttpClient) { }
    getAllCourse(): Observable<Courses> {
            return this.http.get<Courses>('api/course');
        } 

        getEditCourse(id): Observable<Courses> {
            return this.http.get<Courses>('api/course/' + id,{});
        }
        getViewCourse(id): Observable<Courses> {
            return this.http.get<Courses>('api/course/' + id,{});
        }

        saveCourse(req): Observable<Courses>{
            let body = req;
            return this.http.post<Courses>('api/course',body);
        }

        removeCourse(id): Observable<Courses> {
            return this.http.delete<Courses>('api/course/' + id,{});
        }

        editCourse(id, req): Observable<Courses> {
            let body = req;
            delete body.id;
            return this.http.put<Courses>('api/course/' + id,body);
        } 

        enrollCourse(courseId, userId): Observable<Courses> {           
            return this.http.post<Courses>('api/userMappingRoutes',{'courseId':courseId,'userId':userId});
        } 

        schduledCourse(id,req){
            let body = req;
            delete body.id;
            return this.http.put<Courses>('api/schduleCourse/' + id,body);
        }

  }