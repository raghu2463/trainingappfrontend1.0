import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { TablesDataService } from "../table/components/data-table/tablesData.service";
import { Modal } from "ngx-modal";
import swal from "sweetalert2";
import { CoursesService } from "./courses.service";
import { Courses } from './courses.model';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-courses",
  templateUrl: "./courses.component.html",
  styleUrls: ["./courses.component.scss"],
  providers: [TablesDataService],
})
export class CoursesComponent implements OnInit {
  @ViewChild("addCoursetModel") addCoursetModel: Modal;
  @ViewChild("viewCoursetModel") viewCoursetModel: Modal;
  @ViewChild("editCourseModel") editCourseModel: Modal;
  @ViewChild("schduleCourseModel") schduleCourseModel: Modal;
  
  tableData: Array<any>;
  filteredTableData: Array<any>;
  coursesModel: Courses;
  roleCode:any;
  pageSize = 10;
  pageNumber = 1;
  searchText: string;
  courseLoadingSubscription: Subscription;
  constructor(
    private router: Router,
    private coursesService: CoursesService,
    private tablesDataService: TablesDataService
  ) {
    this.coursesModel = {}
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.courseLoadingSubscription = this.coursesService.getAllCourse().subscribe(
      (data) => {
        this.tableData = data["data"];
        this.filteredTableData = this.tableData;
      },
      (error) => {
        console.log(error);
      }
    );
    this.roleCode = localStorage.getItem("roleCode");
  }

  // Data table page cheange method
  pageChanged(pN: number): void {
    this.pageNumber = pN;
    this.pageSize = 10;
    console.log(this.pageNumber);
  }

  searchOption(event) {
    const value = this.searchText;
    this.filteredTableData = Object.assign([], this.tableData).filter(
      (item) => {
        return item.firstName.toLowerCase().indexOf(value.toLowerCase()) > -1;
      }
    );
  }

  closeModal(modal) {
    modal.close();
  }

  addCourse() {
    this.coursesModel = {};
    this.addCoursetModel.open();
  }

  addCourseSubmite(){
  this.coursesService.saveCourse(this.coursesModel).subscribe(
    (data) => {
      this.tableData = data["data"];
      this.filteredTableData = this.tableData;
      swal("Updated", data['message'], "success");
      this.loadData();
      this.addCoursetModel.close();
    },
    (error) => {
      console.log(error);
    }
  );
  }

  viewCourse(id) {
    this.coursesService.getViewCourse(id).subscribe(
      (data) => {
        this.coursesModel = data["data"];
        this.viewCoursetModel.open();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  editCourse(id) {
    this.coursesService.getViewCourse(id).subscribe(
      (data) => {
        this.coursesModel = data["data"];
        this.coursesModel.byUserId = localStorage.getItem('userId');
        this.editCourseModel.open();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  editCourseSubmite(){
    console.log("===============this.coursesModel================>",this.coursesModel.id)
    this.coursesService.editCourse(this.coursesModel.id,this.coursesModel).subscribe(
      (data) => {
        console.log("===============data================>",data)
        swal("Updated", data['message'], "success");
        this.loadData();
        this.editCourseModel.close();  
      },
      (error) => {
        console.log(error);
      }
    );
  }

  enrollCourse(courseId) {
    let userId = localStorage.getItem('userId');
    swal({
      title: "Are you sure",
      text: "Would you like to enroll the course?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        this.coursesService.enrollCourse(courseId,userId).subscribe(
          (data) => {
            swal("Enrolled", data['message'], "success");
            this.loadData();
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
}

  deleteCourse(id) {
    swal({
      title: "Are you sure",
      text: "Would you like to in-activate?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        this.coursesService.removeCourse(id).subscribe(
          (data) => {
            swal("Deleted", data['message'], "success");
            this.loadData();
          },
          (error) => {
            console.log(error);
          }
        );
      }
    });
  }

  dateCalculation(){
    console.log("=====================>",this.coursesModel.fromDate);
    console.log("=====================>",this.coursesModel.toDate);    
  }

  schduleCourse(id){
    this.coursesService.getViewCourse(id).subscribe(
      (data) => {
        console.log("=====================>",data["data"]);
        this.coursesModel = data["data"];
        this.coursesModel.byUserId = localStorage.getItem('userId');
        this.schduleCourseModel.open();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  schduleCourseSubmite(){
    this.coursesService.schduledCourse(this.coursesModel.id,this.coursesModel).subscribe(
      (data) => {
        swal("Schduled", data['message'], "success");
        this.schduleCourseModel.close();
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
