import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Modal } from "ngx-modal";
import { UsersService } from "./users.service";
import { User } from "./user.model";
import swal from 'sweetalert2';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
  providers: [],
})
export class UsersComponent implements OnInit {
  @ViewChild("editUserModel") editUserModel: Modal;
  @ViewChild("viewUserModel") viewUserModel: Modal;

  tableData: Array<any>;
  filteredTableData: Array<any>;
  userModel: User;
  roleCode:any;
  userLoadingSubscription:Subscription
  pageSize = 10;
  pageNumber = 1;
  searchText: string;
  public url: string;
  constructor(
    private router: Router,
    private userService: UsersService
  ) {
    this.userModel = {}  
  }

  ngOnInit() {
    this.loadData();
    this.tableData = [];
  }

  loadData() {
    this.userLoadingSubscription = this.userService.getAllUser().subscribe(
      (data) => {
        this.tableData = data["data"];
        this.filteredTableData = this.tableData;
      },
      (error) => {
        console.log(error);
      }
    );
    this.roleCode = localStorage.getItem("roleCode");
  }

  // Data table page cheange method
  pageChanged(pN: number): void {
    this.pageNumber = pN;
    this.pageSize = 10;
    console.log(this.pageNumber);
  }

  searchOption(event) {
    const value = this.searchText;
    this.filteredTableData = Object.assign([], this.tableData).filter(
      (item) => {
        return item.firstName.toLowerCase().indexOf(value.toLowerCase()) > -1;
      }
    );
  }

  closeModal(modal) {
    modal.close();
  }
  public download(downloadUrl: string,id): void {
    console.log('id',id);
    sessionStorage.setItem('id', id);
    window.open('/certificate', '_blank');
   // this.router.navigate(["/certificate"]);
  }

  editUser(id) {
    console.log("==========edit User==============>",id)
    this.userService.getEditUser(id).subscribe((data) => {
      this.userModel = data['data'];
        /* this.userModel.id = data['data'].id;
        this.userModel.firstName = data['data'].firstName;
        this.userModel.lastName = data['data'].lastName;
        this.userModel.email = data['data'].email;
        this.userModel.phoneNumber = data['data'].phoneNumber;
        this.userModel.headLine = data['data'].headLine;
        this.userModel.roleId = data['data'].roleId;
        this.userModel.linkedIn = data['data'].linkedIn;
        this.userModel.github = data['data'].github;
        this.userModel.youTube = data['data'].youTube; */
          this.editUserModel.open();      
      },
      (error) => {
        console.log(error);
      });    
  }

  viewUser(id){
    console.log("==========view User==============>",id)
    this.userService.getViewUser(id).subscribe(
      (data) => {
        this.userModel.id = data['data'].id;
        this.userModel.firstName = data['data'].firstName;
        this.userModel.lastName = data['data'].lastName;
        this.userModel.email = data['data'].email;
        this.userModel.phoneNumber = data['data'].phoneNumber;
        this.userModel.headLine = data['data'].headLine;
        this.userModel.roleId = data['data'].roleId;
        this.userModel.linkedIn = data['data'].linkedIn;
        this.userModel.github = data['data'].github;
        this.userModel.youTube = data['data'].youTube;
          this.viewUserModel.open();      
      },
      (error) => {
        console.log(error);
      }); 
  }

  updateUser(){   
    this.userService.editUser(this.userModel.id,this.userModel).subscribe(
      (data) => {
        console.log("=====================>",data)
        swal("Updated", data['message'], "success");
        this.editUserModel.close();  
        this.loadData();  
      },
      (error) => {
        console.log(error);
      }); 
  }

  deleteUser(id) {
    swal({
      title: "Are you sure",
      text: "Would you like to remove the user",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        this.userService.removeUser(id).subscribe(
          (data) => {
            swal("Deleted", data['message'], "success");
            this.loadData();
          },
          (error) => {
            console.log(error);
          });       
      }
    });
  }
}
