import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { User } from './user.model';
import {SERVER_API_URL} from '../../constant'

@Injectable({
    providedIn: 'root'
  })
  
  export class UsersService {
    constructor(private http: HttpClient) { }
        getAllUser(): Observable<User> {
            return this.http.get<User>('api/users');
        } 

        getEditUser(id): Observable<User> {
            return this.http.get<User>('api/users/' + id,{});
        }
        getViewUser(id): Observable<User> {
            return this.http.get<User>('api/users/' + id,{});
        }

        removeUser(id): Observable<User> {
            return this.http.delete<User>('api/users/' + id,{});
        }

        editUser(id, req): Observable<User> {
            let body = req;
            delete body.id;
            return this.http.put<User>('api/users/' + id,body);
        } 

  }