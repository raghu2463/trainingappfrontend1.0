export interface User {
    id?: string;

    firstName?: string;

    lastName?: string;

    email?: string;

    phoneNumber?: string;

    headLine?: string; 

    userImage?: string;

    linkedIn?: string;

    github?: string;

    youTube?: string;

    roleId?: string;

    status?: string;

    createdAt?: Date;

    updatedAt?: Date;
}