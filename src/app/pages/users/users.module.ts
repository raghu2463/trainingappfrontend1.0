import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { UsersRouting } from './users.routing';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-modal';
import { UsersComponent } from './users.component';
import { BusyModule } from 'ngx-busy';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        UsersRouting,
        NgxPaginationModule,
        FormsModule,
        ModalModule,
        BusyModule
    ],
    declarations: [
        UsersComponent,
    ]
})
export class UsersModule { }
